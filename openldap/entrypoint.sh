#!/bin/sh

set -ex

CONFIG_DIR="${CONFIG_DIR:-/etc/openldap/slapd.d}"
CONFIG_FILE="${CONFIG_FILE:-cn=config.ldif}"
DATABASE_DIR="${DATABASE_DIR:-/var/lib/openldap/openldap-data}"
KEYTAB_PATH="${KEYTAB_PATH:-/etc/keytab}"
SASL_DIR="${SASL_DIR:-/etc/sasl2}"
SCHEMA_DIR="${SCHEMA_DIR:-/etc/openldap/schema}"
SOCKET_PATH="${SOCKET_PATH:-/run/saslauthd}"
USER="${USER:-openldap}"

export LDAP_PASSWORD="${LDAP_PASSWORD:-"{CRYPT}$$6$$XM9TMkrwfdKGi67X$$qUVYm0lElbOAMsOV5bSp0zNSIFjyZqLPtFBaY00M4J.E4/Nc.MyJQD8XfuSHKbs22QA7d6WdvnR4ExHAnABwf1"}"
export CERTS_PATH="${CERTS_PATH:-/var/lib/acme/ldap.prologin.org}"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH":/usr/local/lib

ulimit -n 1024

if ! id "$USER"; then
    addgroup --system "$USER"
    adduser --system --no-create-home --disabled-password "$USER" "$USER"
fi

for dir in "$CONFIG_DIR" "$SCHEMA_DIR" "$DATABASE_DIR" "$SASL_DIR" "$KEYTAB_PATH" "$CERTS_PATH"; do
    mkdir -p "$dir"
done

mkdir -p "$(dirname "$SOCKET_PATH")"
envsubst <krb5.conf.tpl >/etc/krb5.conf

if [ -n "$SASLAUTHD" ]; then
    cp "$KEYTAB_PATH"/krb5.keytab /etc
    saslauthd -a kerberos5 -m "$SOCKET_PATH" -d
fi

cp rfc2307bis.ldif "$SCHEMA_DIR" # LITTLE CHETOR BUT FLEMME
envsubst <slapd.conf.tpl >"$SASL_DIR"/slapd.conf

if [ -z "$(ls -A "$DATABASE_DIR")" ]; then
    rm -rf "${CONFIG_DIR:-?}"/*
    for file in "bootstrap.ldif" "$CONFIG_FILE"; do
        envsubst <"$file".tpl >/container/"$file"
    done
    slapadd -d '-1' -F "$CONFIG_DIR" -b'cn=config' -l "/container/cn=config.ldif"
    slapadd -d '-1' -F "$CONFIG_DIR" -l "/container/bootstrap.ldif"
    slaptest -u -F "$CONFIG_DIR"
fi

chown -R "$USER":"$USER" "$CONFIG_DIR"
chown -R "$USER":"$USER" "$SASL_DIR"
chown "$USER":"$USER" "$CERTS_PATH"/*
chown -R "$USER":"$USER" "$(dirname "$DATABASE_DIR")"

chmod +x "$(dirname "$SOCKET_PATH")"
chmod 500 "$CERTS_PATH"/*

slapd -u "$USER" -g "$USER" -d '-1' -F "$CONFIG_DIR" -h "ldapi://%2Fvar%2Frun%2Fldapi $BIND_STR"
