dn: dc=prologin,dc=org
objectClass: dcObject
objectClass: organization
dc: prologin
o: Prologin

dn: ou=groups,dc=prologin,dc=org
objectClass: organizationalUnit
ou: groups

dn: ou=users,dc=prologin,dc=org
objectClass: organizationalUnit
ou: users

dn: cn=admin,dc=prologin,dc=org
objectClass: organizationalRole
objectClass: simpleSecurityObject
description: LDAP Administrator
userPassword: ${LDAP_PASSWORD}

dn: cn=roots,ou=groups,dc=prologin,dc=org
objectClass: top
objectClass: groupOfMembers
objectClass: posixGroup
cn: roots
gidNumber: 1000

dn: cn=staffs,ou=groups,dc=prologin,dc=org
objectClass: top
objectClass: groupOfMembers
objectClass: posixGroup
cn: staffs
gidNumber: 2000

dn: cn=participants,ou=groups,dc=prologin,dc=org
objectClass: top
objectClass: groupOfMembers
objectClass: posixGroup
cn: participants
gidNumber: 3000
