dn: cn=config
cn: config
objectClass: olcGlobal
olcLogLevel: -1
olcSaslHost: sasl
olcSaslSecProps: none
olcTLSCACertificateFile: ${CERTS_PATH}/fullchain.pem
olcTLSCertificateFile: ${CERTS_PATH}/fullchain.pem
olcTLSCertificateKeyFile: ${CERTS_PATH}/key.pem
olcTLSProtocolMin: 3.4
olcTLSVerifyClient: never

dn: cn=module,cn=config
cn: module
objectClass: olcModuleList
olcModulePath: /usr/lib/openldap
olcModuleLoad: back_mdb
olcModuleLoad: memberof
olcModuleLoad: refint

dn: cn=schema,cn=config
cn: schema
objectClass: olcSchemaConfig

include: file:///etc/openldap/schema/core.ldif

include: file:///etc/openldap/schema/cosine.ldif

include: file:///etc/openldap/schema/inetorgperson.ldif

include: file:///etc/openldap/schema/rfc2307bis.ldif

dn: olcDatabase={-1}frontend,cn=config
objectClass: olcDatabaseConfig
olcAccess: {0}to dn.base="" by * read
olcAccess: {1}to dn.base="cn=subschema" by * read
olcAccess: {2} to * by dn.exact=uidNumber=0+gidNumber=0,cn=peercred,cn=external,cn=auth manage by * none stop
olcDatabase: {-1}frontend

dn: olcDatabase={0}config,cn=config
objectClass: olcDatabaseConfig
olcAccess: to * by * none break
olcDatabase: {0}config

dn: olcDatabase={1}mdb,cn=config
objectClass: olcDatabaseConfig
objectClass: olcMdbConfig
olcDatabase: {1}mdb
olcDbDirectory: /var/lib/openldap/openldap-data
olcDbIndex: objectClass eq
olcDbIndex: cn pres,eq
olcDbIndex: uid pres,eq
olcSuffix: dc=prologin,dc=org
olcAccess: to attrs=userPassword
    by dn.exact=uidNumber=0+gidNumber=0,cn=peercred,cn=external,cn=auth manage
    by group/groupOfMembers/member="cn=roots,ou=groups,dc=prologin,dc=org" manage
    by dn.exact="cn=admin,dc=prologin,dc=org" manage
    by self read
    by anonymous auth
    by * none
olcAccess: to *
    by dn.exact=uidNumber=0+gidNumber=0,cn=peercred,cn=external,cn=auth manage
    by group/groupOfMembers/member="cn=roots,ou=groups,dc=prologin,dc=org" manage
    by dn.exact="cn=admin,dc=prologin,dc=org" manage
    by users read
    by peername.ip=163.5.0.0%255.255.0.0 read
    by anonymous auth

dn: olcOverlay={0}memberof,olcDatabase={1}mdb,cn=config
objectClass: olcOverlayConfig
objectClass: olcMemberOf
olcOverlay: memberof
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfMembers

dn: olcOverlay={1}refint,olcDatabase={1}mdb,cn=config
objectClass: olcOverlayConfig
olcOverlay: refint
