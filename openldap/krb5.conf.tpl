[libdefaults]
  default_realm = ${REALM}
  dns_lookup_kdc = false
  dns_lookup_realm = true
  rdns = false

[realms]
  ${REALM} = {
    admin_server = ${ADMIN_SERVER}
    default_principal_flags = "${KERBEROS_FLAGS}"
    kdc = ${KDC_SERVER}
  }
