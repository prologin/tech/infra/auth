#!/bin/sh

REALM="${REALM:-PROLOGIN.ORG}"
SASL_KEYTAB="${SASL_KEYTAB:-host/auth}"
ROOT_PASSWORD="${ROOT_PASSWORD:-prologin}"
KDC_FILE_PATH="${KDC_FILE_PATH:-/etc/krb5kdc/kdc.conf}"
KRB5_FILE_PATH="${KRB5_FILE_PATH:-/etc/krb5.conf}"
ACL_FILE_PATH="${ACL_FILE_PATH:-/etc/krb5kdc/PROLOGIN.ORG.acl}"
KEYTAB_PATH="${KEYTAB_PATH:-/run/keytab/krb5.keytab}"
DB_PATH="${DB_PATH:-/var/lib/krb5kdc}"

if [ -z "$(ls -A "$DB_PATH")" ]; then
kdb5_util create -s -r "$REALM" <<EOF
$ROOT_PASSWORD
$ROOT_PASSWORD

EOF
fi


for file in "$KRB5_FILE_PATH" "$KDC_FILE_PATH" "$ACL_FILE_PATH"; do
    mkdir -p "$(dirname "$file")"
    envsubst < "$(basename "$file".tpl)" > "$file"
done


if [ "$#" -eq 0 ]; then
    if [ -n "$KADMIN" ]; then
        mkdir -p "$(dirname "$KEYTAB_PATH")"
        if [ -z "$(ls -A "$(dirname "$KEYTAB_PATH")")" ]; then
            kadmin.local -q "ank -randkey ${SASL_KEYTAB}@${REALM}"
            kadmin.local -q "ktadd -k ${KEYTAB_PATH} ${SASL_KEYTAB}@${REALM}"
        fi
        /usr/sbin/kadmind -nofork
    else
        /usr/sbin/krb5kdc -n
    fi
fi

exec "$@"
